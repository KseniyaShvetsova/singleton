// Pattern.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <conio.h>
#include <iostream>
#include <string>

using namespace std;

class KingSingleton {
public:
	static KingSingleton& Instance();

	string getkingName() { return _kingName; };
	void setkingName(string kingName) { _kingName = kingName; };

private:
    KingSingleton() { _kingName = "KING"; };
    KingSingleton(const KingSingleton&);
	KingSingleton& operator=(const KingSingleton&);
	string _kingName;
};

KingSingleton& KingSingleton::Instance() {
	static KingSingleton _instance;
	return _instance;
};

int main() {
	//������ ������
	KingSingleton& myKing = KingSingleton::Instance();
	myKing.setkingName("Arthur");
	cout << "Get the first instance:" << endl;
	cout << myKing.getkingName() << endl << endl;

	//����� ���� ������ ���� ������, ���� ���� ������ ����� ������
	KingSingleton& newKing = KingSingleton::Instance();
	cout << "The second time's instance is the same:" << endl;
	cout << myKing.getkingName() << endl << endl;
	getch();
	return 0;
}
